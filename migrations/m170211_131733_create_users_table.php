<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170211_131733_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(20)->notNull(),
            'password' => $this->string(255),
            'auth_key' => $this->string(255),
        ]);
        $this->batchInsert('users', ['name', 'password'], [
            ['admin', '$2y$13$UDc06Ceh2CS2qNgYv.AGPe2zckXx95WMlAEQh036tztGQW/doBfym'],
            ['Tim', '$2y$13$.2BwadJHdL3J0uBD8sPrG.jkqEF0n3mUX4u0fnVdGifORtWRaiRgu'], 
            ['Andrew', '$2y$13$CT3RmeWpXk9EPmAXXa44fu4d6dNMuWMDpM6Y1Hq/iM8boEkERpugW'],
            ['Jannie', '$2y$13$bbeIUv.GxUxUAp98jTr5C.Sizj3yIis2wEedEYwr2ly7LqLZGGspa'],
            ['Hank', '$2y$13$FtWpc3tB01Eu3FZkKHRo..w2i5Bc9fs2swUOkFEk/yqtSDxhysjD6'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
