<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 * Has foreign keys to the tables:
 *
 * - `project`
 * - `task_type`
 * - `users`
 */
class m170211_142734_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'task_type_id' => $this->integer()->notNull(),
            'text' => $this->text(),
            'created_at' => $this->datetime(),
            'user_id' => $this->integer()->notNull(),
            'duration' => $this->integer(),
        ]);

        // creates index for column `project_id`
        $this->createIndex(
            'idx-task-project_id',
            'task',
            'project_id'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-task-project_id',
            'task',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );

        // creates index for column `task_type_id`
        $this->createIndex(
            'idx-task-task_type_id',
            'task',
            'task_type_id'
        );

        // add foreign key for table `task_type`
        $this->addForeignKey(
            'fk-task-task_type_id',
            'task',
            'task_type_id',
            'task_type',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-task-user_id',
            'task',
            'user_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-task-user_id',
            'task',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-task-project_id',
            'task'
        );

        // drops index for column `project_id`
        $this->dropIndex(
            'idx-task-project_id',
            'task'
        );

        // drops foreign key for table `task_type`
        $this->dropForeignKey(
            'fk-task-task_type_id',
            'task'
        );

        // drops index for column `task_type_id`
        $this->dropIndex(
            'idx-task-task_type_id',
            'task'
        );

        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-task-user_id',
            'task'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-task-user_id',
            'task'
        );

        $this->dropTable('task');
    }
}
