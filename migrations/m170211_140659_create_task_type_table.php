<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_type`.
 */
class m170211_140659_create_task_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(20)->notNull(),
        ]);
        $this->batchInsert('task_type', ['name', 'id'], [
            ['Задача', 1],
            ['Баг', 2],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task_type');
    }
}
