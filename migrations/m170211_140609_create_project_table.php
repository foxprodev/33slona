<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m170211_140609_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'name' => $this->string(40)->notNull()->unique(),
        ]);
        $this->batchInsert('project', ['name'], [
            ['Музей'],
            ['Приложение стадиона'],
            ['ФСС'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project');
    }
}
