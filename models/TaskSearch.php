<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSearch represents the model behind the search form about `app\models\Task`.
 */
class TaskSearch extends Task
{
    public $username;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'task_type_id', 'user_id', 'duration'], 'integer'],
            ['username', 'string'],
            [['text', 'created_at'], 'safe'],
            ['created_at', 'validateDatetime']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find()->joinWith('project')->joinWith('taskType')->joinWith('user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['username'] = [
            'asc' => ['user.name' => SORT_ASC],
            'desc' => ['user.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'task_type_id' => $this->task_type_id,
            'created_at' => ($this->created_at != '') ? \DateTime::createFromFormat('d.m.Y', $this->created_at)->format('Y-m-d') : '',
            'user_id' => $this->user_id,
            'duration' => $this->duration,
        ]);

        $query->andFilterWhere(['ilike', 'text', $this->text]);
        $query->andFilterWhere(['ilike', 'users.name', $this->username]);

        return $dataProvider;
    }
}
