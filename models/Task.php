<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $task_type_id
 * @property string $text
 * @property string $created_at
 * @property integer $user_id
 * @property integer $duration
 *
 * @property Project $project
 * @property TaskType $taskType
 * @property Users $user
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'task_type_id', 'user_id', 'duration', 'created_at', 'text'], 'required'],
            [['project_id', 'task_type_id', 'user_id'], 'integer'],
            ['duration', function($attribute) {
                $result;
                preg_match('/^(?: ?(\d+)d)?(?: ?(\d+)h)?(?: ?(\d+)m)?$/', $this->duration, $result);
                \Yii::warning($result);
                if (!count($result)) $this->addError($attribute, 'Проверьте формат ввода' );
            }],
            ['created_at', 'validateDatetime'],
            [['text'], 'string', 'max' => 250],
            [['created_at', 'duration'], 'safe'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['task_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['task_type_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'task_type_id' => 'Тип задачи',
            'text' => 'Текст задачи',
            'created_at' => 'Дата начала',
            'user_id' => 'Кому назначена',
            'duration' => 'Оцененное время',
        ];
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $result;
            preg_match('/^(?: ?(\d+)d)?(?: ?(\d+)h)?(?: ?(\d+)m)?$/', $this->duration, $result);
            $this->duration = $result[1]*1440+$result[2]*60+$result[3];
            $this->created_at = \DateTime::createFromFormat('d.m.Y', $this->created_at)->format('Y-m-d');
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskType()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function formatDuration () {
        $d = round($this->duration / 1440);
        $mod = $this->duration - $d * 1440;
        $h = round($mod / 60);
        $s = $mod % 60;
        return ($d > 0 ? "{$d}d " : "") . ($h > 0 ? "{$h}h " : "") . ($m > 0 ? "{$m}m" : "");
    }
    public function validateDatetime ($attribute) {
        if(strlen($this->created_at) < 8) $this->created_at .= "." . date("Y");
        $check = \DateTime::createFromFormat('d.m.Y', $this->created_at);
        if(!$check) $this->addError($attribute, 'Проверьте формат ввода' );
    }
}
