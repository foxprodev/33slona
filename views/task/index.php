<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Задачи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">
    <?php if(Yii::$app->session->getFlash('task-created') == 1): ?>
    <div class="alert alert-success alert-dismissible fade in" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
         <strong>Вы успешно создали задачу</strong> 
    </div>
    <?php endif; ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить задачу', ['create'], ['class' => 'btn btn-primary open-modal', 'title' => 'Добавить задачу']) ?>
    </p>
    <?php
    $projects = \app\models\Project::find()->select(['name', 'id'])->orderBy('name')->indexBy('id')->column();
    
    if (!isset($searchModel->project_id)) {
        reset($projects);
        $searchModel->project_id = key($projects);
    }
    Pjax::begin(); 
    ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'project_id',
                'value' => 'project.name',
                'filter' => \kartik\select2\Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'project_id',
                    'data' => $projects,
                    'options' => ['placeholder' => 'Выберите...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
                'headerOptions'=>['style'=>'width: 200px;']
            ],
            [
                'attribute' => 'task_type_id',
                'value' => 'taskType.name',
                'filter' => \app\models\TaskType::find()->select(['name', 'id'])->orderBy('name')->indexBy('id')->column(),
            ],
            'text:ntext',
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return (new DateTime($model->created_at))->format('d.m.Y');
                },
                'filter' => kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    // 'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'layout'=>'{input}{remove}',
                    'attribute' => 'created_at',
                    'options' => ['style' => 'text-align: right;'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        // 'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                        'altFormat' => 'mm-dd',
                        'todayHighlight' => true
                    ]
                ]),
            ],
            [
                'attribute' => 'username',
                'value' => 'user.name',
                'label' => 'Кому назначена'
            ],
            'duration',
            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}',
                'buttons'=>[
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('yii', 'Update'),
                            'class' => 'open-modal table-link'
                        ]);

                    },
                ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
