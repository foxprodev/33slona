<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
    
    <?= $form->field($model, 'project_id')->widget(Select2::className(), [
            'data' => \app\models\Project::find()->select(['name', 'id'])->orderBy('name')->indexBy('id')->column(),
            'options' => ['placeholder' => ''],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]
    ) ?>

     <?= $form->field($model, 'task_type_id')->widget(Select2::className(), [
            'data' => \app\models\TaskType::find()->select(['name', 'id'])->orderBy('name')->indexBy('id')->column(),
            'options' => ['placeholder' => ''],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]
    ) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6, 'maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

     <?= $form->field($model, 'user_id')->widget(Select2::className(), [
            'data' => \app\models\User::find()->select(['name', 'id'])->orderBy('name')->indexBy('id')->column(),
            'options' => ['placeholder' => ''],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]
    ) ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => 'pull-right ' . ($model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')]) ?>
    </div>
    <div class="clearfix"></div>

    <?php ActiveForm::end(); ?>

</div>
