$(document).on('click', '.open-modal:not([disabled])', function(e) {
    e.preventDefault();

    $('#modalWindow').modal('show')
        .find('#modalBody').html('<p style="text-align:center"> <span>Загрузка...</span></p>')
        .load($(this).attr('href'));

    $('#modalTitle').text($(this).attr('title'));

});